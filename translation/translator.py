#!/usr/bin/python3
# pylint: disable=missing-function-docstring  # чтобы не быть Капитаном Очевидностью
# pylint: disable=import-error  # не видит мои модули
"""Транслятор Assembler в машинный код"""
import sys

from constants.error_msg import DUPLICATE_LABEL, INVALID_OPERAND, MISSING_START_LABEL, TOO_BIG_INPUT
from constants.isa import Term, Operand, OperandType, PRINT_ADDR, READ_ADDR, instr_info
from translation import serialization
from translation.validation import \
    validate_section_line, is_section, \
    validate_operation_line, is_label, \
    validate_operands


def index_labels(raw_code: list):
    """
    Функция индексации всех меток в программе.
    Заполняет словарь соответствия:
        - меток команд - порядковому номеру инструкции;
        - меток данных - номера ячейки памяти, куда будет загружена переменная.
    """
    text_labels: dict = {}
    data_labels: dict = {}

    non_blank_lines_cnt: int = 0
    cur_section: str = ""
    mem_pos: int = 0

    avalible_sections = {"data", "text"}

    for line_num, line in enumerate(raw_code, 1):
        if line.strip() == '' or line.strip()[0] == ";":
            continue

        if not cur_section:
            cur_section = validate_section_line(line, line_num, avalible_sections)
        else:
            if is_label(line):
                if cur_section == "text":
                    assert line[:-1] not in text_labels, DUPLICATE_LABEL.format(line[:-1])
                    text_labels[line[:-1]] = non_blank_lines_cnt
                else:
                    assert line[:-1] not in data_labels, DUPLICATE_LABEL.format(line[:-1])
                    data_labels[line[:-1]] = mem_pos
                    mem_pos += 1
            elif is_section(line):
                cur_section = validate_section_line(line, line_num, avalible_sections)
            else:
                parts = validate_operation_line(line, line_num)
                non_blank_lines_cnt += 1 + instr_info[parts[0]]["operands"]
                if parts[0] in {"print", "read"}:
                    non_blank_lines_cnt += 1
                    # так как физически будет хранить 2 аргумента
    return text_labels, data_labels


def fill_terms(raw_code: list, text_labels: dict, data_labels: dict):
    """
    Функция заполнения списка термов, содержащих имя операции, типы и значения операндов.
    Валидация строк программы и операндов интегрирована сюда намеренно,
    чтобы сократить количество итераций по программе.
    """
    terms: list[Term] = []
    vars_mem_idx = 0

    for line_num, line in enumerate(raw_code, 1):
        if line.strip() == '' or line.strip()[0] == ";" or is_label(line) or is_section(line):
            # Обрабатываем только линии с операциями
            continue
        parts = validate_operation_line(line, line_num)
        instr_name = parts[0]

        term = Term(instr_name)

        for i in range(instr_info[instr_name]["operands"]):
            if parts[1 + i] in list(text_labels.keys()) + list(data_labels.keys()):
                op_type = OperandType.ADDR
            elif parts[1 + i] == "rax":
                op_type = OperandType.REG
            elif parts[1 + i].isdigit():
                op_type = OperandType.NUM
                assert -2147483648 <= int(parts[1 + i]) <= 2147483647, \
                    TOO_BIG_INPUT.format(parts[1 + i])
            else:
                raise AssertionError(INVALID_OPERAND.format(parts[1 + i], line_num))

            term.operands.append(Operand(op_type, parts[1 + i]))

        # Обработка "специфических" инструкций, которым нужно добавить дополнительный операнд.
        # "Спецефические" - не имеющие собственного опкода
        if instr_name == "read":
            term.operands.append(term.operands[0])
            term.operands[0] = (Operand(OperandType.ADDR, READ_ADDR))
        elif instr_name == "print":
            term.operands.append(Operand(OperandType.ADDR, PRINT_ADDR))
        elif instr_name == "word":
            term.operands.append(Operand(OperandType.ADDR, vars_mem_idx))
            vars_mem_idx += 1

        # Валидация соответствия типов операндов их инструкциям.
        try:
            validate_operands(term, text_labels, data_labels)
        except AssertionError as exc:
            raise AssertionError(INVALID_OPERAND.format(term.operands[1].value, line_num)) from exc

        if instr_name == "word":
            #  Инструкции инициализации памяти вставляем в начало
            terms.insert(0, term)
            continue
        terms.append(term)

    assert "start" in text_labels, MISSING_START_LABEL
    terms.insert(vars_mem_idx, Term("jmp", [Operand(OperandType.ADDR, "start")]))

    return terms, vars_mem_idx


def translate(text):
    raw_code = list(text.split('\n'))

    # Запоминаем адреса всех меток
    text_labels, data_labels = index_labels(raw_code)
    # Транслируем текст в последовательность значимых термов.
    terms, vars_count = fill_terms(raw_code, text_labels, data_labels)

    # Транслируем термы в машинный код.
    code = []
    for term in terms:
        for operand in term.operands:
            cur_op = term.operands[term.operands.index(operand)]
            if operand.type == OperandType.ADDR and not str(operand.value).isdigit():
                if operand.value in text_labels:
                    cur_op.value = text_labels[operand.value] + 3 * vars_count + 2
                    # добавляем отступ, так как в начале идут команды инициализации памяти
                    # и переход в метку start
                else:
                    cur_op.value = data_labels[operand.value]
            elif operand.type == OperandType.REG:
                cur_op.value = 0

        code.append(serialization.serialize_term(term, instr_info))
    return code


def main(args):
    """Функция запуска транслятора."""
    assert len(args) == 2, \
        "Wrong arguments: translator.py <asm_file> <target>"
    source, target = args

    with open(source, "rt", encoding="utf-8") as file:
        source = file.read()

    code = translate(source)
    print("source LoC:", len(source.split("\n")), "code instr:", len(code))
    with open(target, "w", encoding="utf-8") as file:
        for instr in code:
            file.write(instr)


if __name__ == '__main__':
    sys.path.append('.')
    main(sys.argv[1:])
