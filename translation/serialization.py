# pylint: disable=missing-function-docstring  # чтобы не быть Капитаном Очевидностью
# pylint: disable=import-error
"""Модуль сериализации (десереализации) списка термов в бинарный машинный код"""
import sys

from constants.isa import Term, OperandType, Opcode


def get_bin(num, bits):
    """Получить двоичную bits-разрядную запись числа num"""
    return format(int(num), 'b').zfill(bits)


def serialize_term(term: Term, instr_info: dict) -> str:
    result = get_bin(instr_info[term.operation]["opcode"], 4)
    op_count = len(term.operands)
    for operand in term.operands:
        result += get_bin(operand.type, 2)
    result += get_bin(OperandType.NONE, 2) * (2 - op_count)
    for operand in term.operands:
        if operand.type in {OperandType.NUM, OperandType.ADDR}:
            result += get_bin(operand.value, 32)
    return result


def serialize_vars(vars_mem: list[int]) -> list[str]:
    result = [get_bin(len(vars_mem), 8)]
    for var in vars_mem:
        result.append(get_bin(var, 32))
    return result


def deserialize_code(code: str):
    # code, instr_list = deserialize_vars(code)
    instr_list = []
    while code:
        instr: dict = {"opcode": Opcode(int(code[:4], 2)), "operands": []}
        code = cut_str_front(code, 4)

        for i in range(2):
            instr["operands"].append(OperandType(int(code[:2], 2)))
            code = cut_str_front(code, 2)
        instr_list.append(instr)

        for i in range(2):
            if instr["operands"][i] in {OperandType.NUM, OperandType.ADDR}:
                op_val = int(code[:32], 2)
                instr_list.append(op_val)  # type: ignore
                code = cut_str_front(code, 32)

    return instr_list


def cut_str_front(string: str, lenght_to_cut) -> str:
    return string[lenght_to_cut:]


def main(args):
    assert len(args) == 1, "Wrong argument: serialization.py <code_file>"
    with open(args[0], encoding="utf-8") as file:
        instr_list = deserialize_code(file.read())
        for _, instr in enumerate(instr_list):
            try:
                bytes_cnt = 1
                operands = instr["operands"]
                print(instr, end=",")
                for operand in operands:
                    if operand is OperandType.NONE:
                        continue
                    print(operand.value, end="; ")
                    if operand in {OperandType.NUM, OperandType.ADDR}:
                        bytes_cnt += 4
            except TypeError:
                continue
            print(f"\t{bytes_cnt} bytes")


if __name__ == '__main__':
    main(sys.argv[1:])
