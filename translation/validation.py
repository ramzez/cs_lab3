# pylint: disable=missing-function-docstring
# pylint: disable=missing-module-docstring
# pylint: disable=import-error  # не видит мои модули
import re

from constants.error_msg import \
    UNKNOWN_OPERATION, MULTIPLE_SEMICOLON, INVALID_SYNTAX, \
    WRONG_ARGS_NUM, UNKNOWN_SECTION
from constants.isa import OperandType, instr_info


def is_label(string) -> bool:
    return re.match(r"^[a-z_]+:$", string) is not None


def is_section(string) -> bool:
    return re.match(r"^section \.[a-z]+$", string) is not None


patterns = [r"^\t[a-z]+ *$", r"^\t[a-z]+ [a-z0-9_]+ *$", r"^\t[a-z]+ [a-z0-9_]+, [a-z0-9_]+ *$"]


def validate_operation_line(line, line_num) -> list:
    command = line.split(";")[0]
    parts = line.split()
    instr = parts[0]

    assert instr in instr_info, UNKNOWN_OPERATION.format(line.split()[0], line_num)
    assert len(command.split()) - 1 == instr_info[instr]["operands"], \
        WRONG_ARGS_NUM.format(instr, line_num, instr_info[instr]['operands'], command)
    assert len(line.split(";")) <= 2, MULTIPLE_SEMICOLON.format(line_num)
    assert re.match(patterns[instr_info[instr]["operands"]], command), \
        INVALID_SYNTAX.format(line_num)
    if instr_info[parts[0]]["operands"] == 2:
        parts[1] = parts[1][:-1]
        # удаляем запятую после первого аргумента

    return parts[:instr_info[instr]["operands"] + 1]


def validate_section_line(line, line_num, avalible_sections) -> str:
    assert is_section(line), INVALID_SYNTAX.format(line_num)
    section = line.split(".")[1]
    assert section in avalible_sections, UNKNOWN_SECTION.format(section, line_num)
    avalible_sections.remove(section)
    return section


def validate_operands(term, text_labels: dict, data_labels: dict):
    if term.operation in {"add", "sub"} \
            and term.operands[1].type not in {OperandType.ADDR, OperandType.REG}:
        raise AssertionError
    if term.operation in {"bne", "beq", "jmp"} \
            and (term.operands[0].value not in text_labels.keys()
                 or term.operands[0].type is not OperandType.ADDR):
        raise AssertionError
    if term.operation == "read" and \
            (term.operands[1].type not in {OperandType.ADDR, OperandType.REG}
             or term.operands[1].type is OperandType.ADDR
             and term.operands[1].value not in data_labels.keys()):
        raise AssertionError
    if term.operation == "print" \
            and term.operands[0].type is OperandType.ADDR \
            and term.operands[0].value not in data_labels.keys():
        raise AssertionError
