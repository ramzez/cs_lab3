# pylint: disable=missing-class-docstring
# pylint: disable=too-few-public-methods
"""
Типы данных для представления и сериализация/десериализация машинного кода.
"""

from enum import Enum

# Зарезервированные адреса, куда подключены потоки ввода-вывода
PRINT_ADDR: int = 255
READ_ADDR: int = 254


class Opcode(int, Enum):
    """Коды операций, представленных на уровне языка."""
    BEQ = 0b0000
    BNE = 0b0001
    MOV = 0b0010
    DIV = 0b0011
    MUL = 0b0100
    MOD = 0b0101
    CMP = 0b0110
    ADD = 0b0111
    SUB = 0b1000
    JMP = 0b1001
    EXIT = 0b1010


# словарь символов ЯП с метаданными
instr_info: dict = {"add": {"opcode": Opcode.ADD, "operands": 2},
                    "sub": {"opcode": Opcode.SUB, "operands": 2},
                    "mov": {"opcode": Opcode.MOV, "operands": 2},
                    "div": {"opcode": Opcode.DIV, "operands": 2},
                    "mod": {"opcode": Opcode.MOD, "operands": 2},
                    "mul": {"opcode": Opcode.MUL, "operands": 2},
                    "cmp": {"opcode": Opcode.CMP, "operands": 2},
                    "beq": {"opcode": Opcode.BEQ, "operands": 1},
                    "bne": {"opcode": Opcode.BNE, "operands": 1},
                    "jmp": {"opcode": Opcode.JMP, "operands": 1},
                    "exit": {"opcode": Opcode.EXIT, "operands": 0},
                    "word": {"opcode": Opcode.MOV, "operands": 1},
                    "print": {"opcode": Opcode.MOV, "operands": 1},
                    "read": {"opcode": Opcode.MOV, "operands": 1}}


class OperandType(int, Enum):
    NONE = 0
    REG = 1
    NUM = 2
    ADDR = 3


class Operand:
    def __init__(self, op_type: int, value):
        self.type = op_type
        self.value = value


class Term:
    """Описание выражения из исходного текста программы."""

    def __init__(self, operation: str, operands=None):
        if operands is None:
            operands = []
        self.operation: str = operation
        self.operands: list[Operand] = operands
