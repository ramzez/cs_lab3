#!/usr/bin/python3
# pylint: disable=missing-function-docstring
# pylint: disable=invalid-name
# pylint: disable=consider-using-f-string
# pylint: disable=import-error
# pylint: disable=too-few-public-methods
"""Модель процессора, позволяющая выполнить странслированные программы на языке Assembler."""

import logging
import sys

from translation import serialization
from constants.error_msg import \
    TOO_BIG_INPUT, SEG_FAULT, INTERNAL_ERR, \
    NEGATIVE_SIZE_ERR, TOO_LONG_EXEC
from constants.isa import Opcode, OperandType, PRINT_ADDR, READ_ADDR


class Alu:
    """Арифметико-логическое устройство с двумя входами данных и сигналом операции."""

    def __init__(self):
        self.left: int = 0
        self.right: int = 0
        self.operations: dict = {
            Opcode.DIV: lambda left, right: left / right,
            Opcode.MOD: lambda left, right: left % right,
            Opcode.CMP: lambda left, right: left - right,
            Opcode.ADD: lambda left, right: left + right,
            Opcode.SUB: lambda left, right: right - left,
            Opcode.MUL: lambda left, right: left * right
        }


def ticked(f):
    def wrapper(*args):
        args[0].tick()
        return f(*args)

    return wrapper


class DataPath:
    """Тракт данных (пассивный), включая: ввод/вывод, память и арифметику.

    - data_memory -- однопортовая, поэтому либо читаем, либо пишем.
    - input/output -- токенизированная логика ввода-вывода. Не детализируется.
    - input -- может вызвать остановку процесса моделирования,
      если буфер входных значений закончился.

    Реализованные методы соответствуют группам активированных сигналов.
    Сигнал "исполняется" за один такт. Корректность использования сигналов за
    один такт -- задача ControlUnit.
    """

    def __init__(self, data_memory_size, input_buffer):
        assert data_memory_size > 0, NEGATIVE_SIZE_ERR
        self.data_memory: list[int] = [0] * data_memory_size
        self.data_address: int = 0
        self.acc: int = 0
        self.input_buffer: list[str] = input_buffer
        self.output_buffer: list[str] = []
        self.alu: Alu = Alu()
        self.operand: int = 0

    def latch_data_addr(self, operand_sel: bool):
        if operand_sel:
            self.data_address = self.operand
        else:
            self.data_address += 1

        assert 0 <= self.data_address < len(self.data_memory), \
            "out of memory: {}".format(self.data_address)

    def latch_acc(self, mem_sel: OperandType):
        """Вывести слово из памяти данных, либо операнд из памяти команд и
         защёлкнуть его в аккумулятор."""
        assert mem_sel in {OperandType.ADDR, OperandType.NUM}, INTERNAL_ERR.format("acc")
        if mem_sel is OperandType.ADDR:
            assert self.data_address != PRINT_ADDR, SEG_FAULT.format(self.data_address)
            if self.data_address == READ_ADDR:
                # memory-mapped input
                if len(self.input_buffer) == 0:
                    raise EOFError()
                symbol = self.input_buffer.pop(0)
                symbol_code = ord(symbol)
                assert -2147483648 <= symbol_code <= 2147483647, TOO_BIG_INPUT.format(symbol_code)
                logging.debug('input: %s', repr(symbol))
                self.acc = symbol_code
            else:
                self.acc = self.data_memory[self.data_address]
        else:
            self.acc = self.operand

    def latch_alu(self, sel_left, mem_sel: OperandType):
        assert mem_sel in {OperandType.ADDR, OperandType.NUM, OperandType.REG}, \
            INTERNAL_ERR.format("alu")
        if sel_left:
            if mem_sel is OperandType.ADDR:
                self.alu.left = self.data_memory[self.data_address]
            else:
                self.alu.left = self.operand
        else:
            self.alu.right = self.acc

    def execute_alu(self, opcode: Opcode):
        res = self.alu.operations[opcode](self.alu.left, self.alu.right)
        while res > 2147483647:
            res = -2147483648 + (res - 2147483647)
        while res < -2147483648:
            res = 2147483647 - (res + 2147483648)

        self.acc = res

    def wr(self):
        """wr (от WRite), сохранить в память."""
        assert self.data_address != READ_ADDR, SEG_FAULT.format(self.data_address)
        if self.data_address == PRINT_ADDR:
            # memory-mapped output
            symbol = self.acc if self.acc > 256 else chr(self.acc)
            logging.debug('output: %s << %s', repr(
                ''.join(self.output_buffer)), repr(symbol))
            self.output_buffer.append(str(symbol))
        else:
            self.data_memory[self.data_address] = self.acc

    def zero(self):
        """Флаг"""
        return self.acc == 0


class ControlUnit:
    """Блок управления процессора. Выполняет декодирование инструкций и
    управляет состоянием процессора, включая обработку данных (DataPath).
    """

    def __init__(self, program, data_path):
        self.program: list = program
        self.program_counter: int = 0
        self.data_path: DataPath = data_path
        self._tick: int = 0

    def tick(self) -> None:
        """Счётчик тактов процессора. Вызывается при переходе на следующий такт."""
        self._tick += 1

    def current_tick(self):
        return self._tick

    def latch_program_counter(self, sel_next: bool) -> None:
        if sel_next:
            self.program_counter += 1
        else:
            self.program_counter = self.program[self.program_counter]

    def set_alu_val(self, is_left: bool, op_type: OperandType):
        self.latch_program_counter(sel_next=True)
        self.tick()
        if op_type is OperandType.REG:
            self.data_path.latch_alu(False, mem_sel=op_type)
            self.tick()
        elif op_type in {OperandType.ADDR, OperandType.NUM}:
            self.data_path.operand = self.program[self.program_counter]
            # w/o tick because "operand" val isn't a physical register,
            # just a way of moving data from ControlUnit.program to DataPath

            if op_type is OperandType.ADDR:
                self.data_path.latch_data_addr(operand_sel=True)
                self.tick()
            if not is_left:
                self.data_path.latch_acc(op_type)
                self.tick()
            self.data_path.latch_alu(is_left, op_type)
            self.tick()

            self.data_path.operand = 0
            # returning zero value as it's not the register

    def decode_and_execute_instruction(self):
        instr: dict = self.program[self.program_counter]
        opcode: Opcode = instr["opcode"]
        operands: list[OperandType] = instr["operands"]

        if opcode is Opcode.EXIT:
            raise StopIteration()
        if opcode is Opcode.JMP:
            self.latch_program_counter(sel_next=True)
            self.tick()
            self.latch_program_counter(sel_next=False)
            self.tick()
        elif opcode is Opcode.MOV:

            for i in range(2):
                op_type = OperandType(operands[i])
                self.latch_program_counter(sel_next=True)
                self.tick()
                if op_type in {OperandType.NUM, OperandType.ADDR}:
                    self.data_path.operand = self.program[self.program_counter]
                    if op_type == OperandType.ADDR:
                        self.data_path.latch_data_addr(operand_sel=True)
                        self.tick()
                    if i == 0:
                        self.data_path.latch_acc(op_type)
                    else:
                        self.data_path.wr()
                    self.tick()
            self.latch_program_counter(sel_next=True)
            self.tick()

        elif opcode in {Opcode.BEQ, Opcode.BNE}:
            # elegant xor
            if self.data_path.zero() ^ opcode:
                self.latch_program_counter(sel_next=True)
                self.tick()
                self.latch_program_counter(sel_next=False)
                self.tick()
                return
            self.latch_program_counter(sel_next=True)
            self.tick()
            self.latch_program_counter(sel_next=True)
            self.tick()
        else:
            self.set_alu_val(is_left=True, op_type=OperandType(operands[0]))
            self.set_alu_val(is_left=False, op_type=OperandType(operands[1]))
            self.data_path.execute_alu(opcode)
            self.tick()
            if opcode in {Opcode.ADD, Opcode.SUB} and OperandType(operands[1]) is OperandType.ADDR:
                # these operations return result in second arg
                self.data_path.wr()
                self.tick()
            self.latch_program_counter(sel_next=True)
            self.tick()

    def __repr__(self):
        state = "{{TICK: {}, PC: {}, ADDR: {}, OUT: {}, ACC: {}}}".format(
            self._tick,
            self.program_counter,
            self.data_path.data_address,
            self.data_path.data_memory[self.data_path.data_address],
            self.data_path.acc,
        )

        instr = self.program[self.program_counter]
        opcode = instr["opcode"]
        args = []
        for i in range(2):
            if instr["operands"][i] != OperandType.NONE:
                args.append(self.program[self.program_counter + i + 1])
        action = "{} {}".format(Opcode(opcode).name, str(args))

        return "{} {}".format(action, state)


def simulation(code, input_tokens, data_memory_size, limit):
    """Запуск симуляции процессора.

    Длительность моделирования ограничена количеством выполненных инструкций.
    """
    data_path = DataPath(data_memory_size, input_tokens)
    control_unit = ControlUnit(code, data_path)
    instr_counter = 0

    try:
        logging.debug('%s', control_unit)
        while True:
            assert limit > instr_counter, TOO_LONG_EXEC
            control_unit.decode_and_execute_instruction()
            instr_counter += 1
            logging.debug('%s', control_unit)
    except EOFError:
        logging.warning('Input buffer is empty!')
    except StopIteration:
        pass
    logging.info('output_buffer: %s', repr(''.join(data_path.output_buffer)))
    return ''.join(data_path.output_buffer), instr_counter, control_unit.current_tick()


def main(args):
    assert len(args) == 2, "Wrong arguments: machine.py <code_file> <input_file>"
    code_file, input_file = args

    with open(code_file, encoding="utf-8") as file:
        raw_code = file.read()

    code = serialization.deserialize_code(raw_code)
    with open(input_file, encoding="utf-8") as file:
        input_text = file.read()
        input_token = []
        for char in input_text:
            input_token.append(char)

    output, instr_counter, ticks = simulation(code,
                                              input_tokens=input_token,
                                              data_memory_size=256, limit=10000)

    print(''.join(output))
    print("instr_counter: ", instr_counter, "ticks:", ticks)


if __name__ == '__main__':
    logging.getLogger().setLevel(logging.DEBUG)
    main(sys.argv[1:])
