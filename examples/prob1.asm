section .text
sum:
	add counter, res
	jmp end

start:
	add 1, counter
	mod counter, 3
	beq sum
	mod counter, 5
	beq sum
end:
	cmp counter, max
	bne start
	print res
	exit

section .data
max:
	word 999
res:
	word 0
counter:
	word 0