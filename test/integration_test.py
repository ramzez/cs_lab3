# pylint: disable=missing-class-docstring     # чтобы не быть Капитаном Очевидностью
# pylint: disable=missing-function-docstring  # чтобы не быть Капитаном Очевидностью
# pylint: disable=line-too-long               # строки с ожидаемым выводом

"""Интеграционные тесты транслятора и машины
"""

import contextlib
import io
import os
import tempfile
import unittest

import machine
from translation import translator


class TestMachine(unittest.TestCase):

    def test_hello(self):
        # Создаём временную папку для скомпилированного файла. Удаляется автоматически.
        with tempfile.TemporaryDirectory() as tmpdirname:
            source = "examples/hello_world.asm"
            target = os.path.join(tmpdirname, "machine_code.out")

            with contextlib.redirect_stdout(io.StringIO()) as stdout:
                translator.main([source, target])
                machine.main([target, target])
                # duplicate target because we don't need input stream

            self.assertEqual(stdout.getvalue(),
                             'source LoC: 35 code instr: 23\nhello world!\ninstr_counter:  22 ticks: 140\n')

    def test_cat(self):
        with tempfile.TemporaryDirectory() as tmpdirname:
            source = "examples/cat.asm"
            target = os.path.join(tmpdirname, "machine_code.out")
            input_stream = "examples/input.txt"

            with contextlib.redirect_stdout(io.StringIO()) as stdout:
                # Собираем журнал событий по уровню INFO в переменную logs.
                with self.assertLogs('', level='INFO') as logs:
                    translator.main([source, target])
                    machine.main([target, input_stream])

            self.assertEqual(stdout.getvalue(),
                             'source LoC: 11 code instr: 6\nHello World from input!\ninstr_counter:  71 ticks: 378\n')

            self.assertEqual(logs.output,
                             ['WARNING:root:Input buffer is empty!',
                              "INFO:root:output_buffer: 'Hello World from input!'"])

    def test_cat_trace(self):
        with tempfile.TemporaryDirectory() as tmpdirname:
            source = "examples/cat.asm"
            target = os.path.join(tmpdirname, "machine_code.out")
            input_stream = "examples/foo_input.txt"

            with contextlib.redirect_stdout(io.StringIO()) as stdout:
                with self.assertLogs('', level='DEBUG') as logs:
                    translator.main([source, target])
                    machine.main([target, input_stream])

            self.assertEqual(stdout.getvalue(),
                             'source LoC: 11 code instr: 6\nfoo\n\ninstr_counter:  14 ticks: 74\n')

            expect_log = [
                "DEBUG:root:MOV [0, 0] {TICK: 0, PC: 0, ADDR: 0, OUT: 0, ACC: 0}",
                "DEBUG:root:JMP [5] {TICK: 6, PC: 3, ADDR: 0, OUT: 0, ACC: 0}",
                "DEBUG:root:MOV [254, 0] {TICK: 8, PC: 5, ADDR: 0, OUT: 0, ACC: 0}",
                "DEBUG:root:input: 'f'",
                "DEBUG:root:MOV [0, 255] {TICK: 15, PC: 8, ADDR: 0, OUT: 102, ACC: 102}",
                "DEBUG:root:output: '' << 'f'",
                "DEBUG:root:JMP [5] {TICK: 22, PC: 11, ADDR: 255, OUT: 0, ACC: 102}",
                "DEBUG:root:MOV [254, 0] {TICK: 24, PC: 5, ADDR: 255, OUT: 0, ACC: 102}",
                "DEBUG:root:input: 'o'",
                "DEBUG:root:MOV [0, 255] {TICK: 31, PC: 8, ADDR: 0, OUT: 111, ACC: 111}",
                "DEBUG:root:output: 'f' << 'o'",
                "DEBUG:root:JMP [5] {TICK: 38, PC: 11, ADDR: 255, OUT: 0, ACC: 111}",
                "DEBUG:root:MOV [254, 0] {TICK: 40, PC: 5, ADDR: 255, OUT: 0, ACC: 111}",
                "DEBUG:root:input: 'o'",
                "DEBUG:root:MOV [0, 255] {TICK: 47, PC: 8, ADDR: 0, OUT: 111, ACC: 111}",
                "DEBUG:root:output: 'fo' << 'o'",
                "DEBUG:root:JMP [5] {TICK: 54, PC: 11, ADDR: 255, OUT: 0, ACC: 111}",
                "DEBUG:root:MOV [254, 0] {TICK: 56, PC: 5, ADDR: 255, OUT: 0, ACC: 111}",
                "DEBUG:root:input: '\\n'",
                "DEBUG:root:MOV [0, 255] {TICK: 63, PC: 8, ADDR: 0, OUT: 10, ACC: 10}",
                "DEBUG:root:output: 'foo' << '\\n'",
                "DEBUG:root:JMP [5] {TICK: 70, PC: 11, ADDR: 255, OUT: 0, ACC: 10}",
                "DEBUG:root:MOV [254, 0] {TICK: 72, PC: 5, ADDR: 255, OUT: 0, ACC: 10}",
                "WARNING:root:Input buffer is empty!",
                "INFO:root:output_buffer: 'foo\\n'"
            ]

            self.assertEqual(logs.output, expect_log)
