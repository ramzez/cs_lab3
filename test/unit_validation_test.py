# pylint: disable=missing-class-docstring     # чтобы не быть Капитаном Очевидностью
# pylint: disable=missing-function-docstring  # чтобы не быть Капитаном Очевидностью
# pylint: disable=import-error  # не видит мои модули
# pylint: disable=undefined-variable
# pylint: disable=wildcard-import
# pylint: disable=trailing-whitespace
# pylint: disable=trailing-newlines


"""Интеграционные тесты транслятора и машины
"""

import unittest

from constants.isa import Term, Operand, OperandType, READ_ADDR, PRINT_ADDR
from translation.validation import validate_section_line, validate_operation_line, validate_operands


class TestValidator(unittest.TestCase):

    def test_section_validation(self):
        validate_section_line("section .text", 0, ["text", "data"])
        with self.assertRaises(AssertionError):
            validate_section_line("section .text", 0, ["data"])
        with self.assertRaises(AssertionError):
            validate_section_line("section ,text", 0, ["text", "data"])

    def test_operation_validation(self):
        res = validate_operation_line("\tadd 1, rax   ; comment", 0)
        self.assertEqual(["add", "1", "rax"], res)

        fakes = ["add 1, rax", "\tadd rax", "\tplus 1, rax", "exit wow, wow",
                 "\tadd 1, rax ; comment ; oh", "\tdiv rax rax"]

        for fake in fakes:
            with self.assertRaises(AssertionError):
                validate_operation_line(fake, 0)

    def test_operands_validation(self):
        text_labels = {"start": 0}
        data_labels = {"var": 0, "var2": 1}
        term = Term("add", [Operand(OperandType.NUM, 1), Operand(OperandType.REG, 0)])

        validate_operands(term, text_labels, data_labels)

        fakes = [Term("add", [Operand(OperandType.NUM, 1), Operand(OperandType.NUM, 0)]),
                 Term("jmp", [Operand(OperandType.ADDR, "weeee")]),
                 Term("jmp", [Operand(OperandType.REG, "rax")]),
                 Term("read", [Operand(OperandType.ADDR, READ_ADDR),
                               Operand(OperandType.NUM, 0)]),
                 Term("print", [Operand(OperandType.ADDR, "var3"),
                                Operand(OperandType.ADDR, PRINT_ADDR)])]
        for fake in fakes:
            with self.assertRaises(AssertionError):
                validate_operands(fake, text_labels, data_labels)
